#ifndef RC2STR_H
#define RC2STR_H
#include <CL/cl.h>
#include <string>
void rcclCreateContextFromType2str(const cl_int rc, std::string &s);
#endif
