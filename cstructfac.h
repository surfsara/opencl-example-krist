#ifdef __cplusplus
 extern "C" {
#endif
void cstructfac(const size_t na, const size_t nr, float*a, float*h, 
                            float*E, double *time, float*v, int times);
#ifdef __cplusplus
}
#endif
