#include <iostream>
#include "cstructfac.h"
#include "clocks.h"
#include "initialize_gpu.h"

using namespace std;

// this is the code to transport data to and from the gpu and
// start a sufficient number of threads, each running 
// the function cstructfac:
//
// We want this function to be callable from C,
// hence extern "C"
//
extern "C" void cstructfac(const size_t na, // input: number of atoms
                           const size_t nr, // input: number of reflections
                           float*a,         // input: atoms. dimension 4*na
                                            //        atomic nr, x,y,z
                           float*hh,        // input: miller indices, 
                                            //        dimension 3*nr
                                            //        h,k,l
                           float*E,         // output: structure factors
                                            //         dimension 2*nr
                                            //         Re, Im
                           double *time,    // output: walltime used
                           float*v,         // inout:  for debuggng purposes
                           int times        // input: the number of times the computation
                                            //        must be done
                          )
{
  //
  // It is advantageous to use float 4 in stead of 3 floats.
  // We make a copy of hh to appropriate array h
  //
   float*h = new float[4*nr];
   for (unsigned int i=0; i<nr; i++)
   {
     h[4*i  ] = hh[3*i  ];
     h[4*i+1] = hh[3*i+1];
     h[4*i+2] = hh[3*i+2];
   }

   //
   // deva, devh devE and devv are kind of pointers to arrays 
   // on the device
   //
   cl_mem deva,devh,devE,devv;
   cl_int rc;
   //
   // allocate devh (copy of h) on the gpu:
   //
   devh = clCreateBuffer(context,  // context
                         CL_MEM_READ_ONLY | 
                         CL_MEM_COPY_HOST_PTR, // define that on the gpu
                                               // this array is read-only
                                               // and that a copy has to be made
                                               // from the array h in main memory
                         nr*sizeof(cl_float4), // number of bytes to allocate and copy
                         h,                    // source to copy from
                         &rc                   // error code
                        );
   if (devh == 0 || rc != 0)
     {
        printf("Cannot allocate memory for h on device\n");
        exit(1);
     }

   //
   // allocate devE (copy of E) on the gpu:
   //
   devE = clCreateBuffer(context,              // context
                         CL_MEM_WRITE_ONLY,    // write-only for the gpu
                         nr*sizeof(cl_float2), // number of bytes to allocate
                         0,                    // array to copy from, not used here
                         &rc                   // error code
                        );
   if (devE == 0 || rc != 0)
     {
        printf("Cannot allocate memory for E on device\n");
        exit(1);
     }

   //
   // fill dubug array v with known values:
   //
   for (int i=0; i<1000; i++)
     v[i]=-i;
   //
   // allocate devv (v on the cpu) on the gpu, and make a copy:
   //

   devv = clCreateBuffer(context,
                         CL_MEM_COPY_HOST_PTR, // we want v on the gpu read/write
                         1000*sizeof(cl_float),v,&rc);
   if (devv == 0 || rc != 0)
     {
        printf("Cannot allocate memory for v on device\n");
        exit(1);
     }
   //
   // allocate array a on the device
   //
   deva = clCreateBuffer(context,
                         CL_MEM_READ_ONLY, // a is read-only on the device
                         na*sizeof(cl_float4),0,&rc);

   if (deva == 0 || rc != 0)
     {
        printf("Cannot allocate memory for a on device\n");
        exit(1);
     }

   //
   // It is best to define the number of threads in a block (group)
   // and take care that the total number of threads is equal to
   // an integer number times the number of threads in a block
   //

   //
   // number of threads in a group:
   // 
   size_t wlsize = max_work_group_size;
   //
   // we want at least nr threads:
   //
   size_t wgsize = (nr / wlsize) * wlsize;  // total number of threads
   if (wgsize < nr) wgsize = wgsize + wlsize;

   cout << "Nr of threads in group   : " << wlsize << endl;
   cout << "Total number of threads  : " << wgsize << endl;

   //
   // define the parameters for the gpu function;
   //
   rc  = clSetKernelArg(kernel, 0, sizeof(cl_int), &na);
   rc |= clSetKernelArg(kernel, 1, sizeof(cl_int), &nr);
   rc |= clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&deva);
   rc |= clSetKernelArg(kernel, 3, sizeof(cl_mem), (void*)&devh);
   rc |= clSetKernelArg(kernel, 4, sizeof(cl_mem), (void*)&devE);
   rc |= clSetKernelArg(kernel, 5, sizeof(cl_mem), (void*)&devv);

   if (rc != CL_SUCCESS)
   {
     cout << "problems with defining parameters for kernel" << endl;
     exit(1);
   }

   //
   // call clstructfac, inclusive the copying of
   // data a number of times:
   // In real life: the array h is fixed, so that one is copied to
   // the gpu once. The array a will change between calls, so
   // we copy that every time.
   //
   double t0 = wallclock();
   for (int tt = 0; tt < times; tt++)
   {

     cl_int rc =
     clEnqueueWriteBuffer(cmdqueue, // command queue
                          deva,     // array to write to
                          CL_TRUE,  // blocking write: we want the
                                    // function to return only after
                                    // all data has been transferred
                          0,        // offset
                          4*na*sizeof(cl_float), // number of bytes to write
                          a,        // source array
                          0,        // events in wait list 
                          0,        // events wait list (not used)
                          0         // event (not used)
                         );
     if (rc != CL_SUCCESS)
     {
       cout << "cannot write 'a' to device" << endl;
       exit(1);
     }

     //
     // Finally! call the function on de gpu device
     //
     rc =
     clEnqueueNDRangeKernel(cmdqueue, // command queue
                            kernel,   // kernel
                            1,        // dimensionality of work-items
                            0,        // global-work-offset, must be NULL
                            &wgsize,  // global-work-size: total number of threads
                            &wlsize,  // local-work-size: number of threads in a block
                            0,        // events-in-wait-list
                            0,        // event-wait-list (not used)
                            0         // event (not used)
                           );
     if (rc != CL_SUCCESS)
     {
       cout << "cannot execute the kernel" << endl;
       exit(1);
     }
     //
     // read the result (E) from the device:
     //
     rc =
     clEnqueueReadBuffer(cmdqueue,              // command queue
                         devE,                  // source
                         CL_TRUE,               // blocked
                         0,                     // offset
                         nr*2*sizeof(cl_float), // number of bytes to copy
                         E,                     // destination
                         0,                     // events in wait-list
                         0,                     // wait list (not used)
                         0                      // event (not used)
                        );
     if (rc != CL_SUCCESS)
     {
       cout << "cannot read 'E' from device" << endl;
       exit(1);
     }

   }
   //
   // timing information:
   //
   double dt = (wallclock() - t0)/times;
   //
   // read the debug array v:
   //
   rc =
   clEnqueueReadBuffer(cmdqueue, // command queue
                       devv,     // source
                       CL_TRUE,  // blocked
                       0,        // offset
                       1000*sizeof(cl_float), // number of bytes to copy
                       v,        // destination
                       0,        // events-in-wait-list
                       0,        // event-wait-list (not used)
                       0         // event (not used)
                      );
   if (rc != CL_SUCCESS)
   {
     cout << "cannot read 'v' from device" << endl;
     exit(1);
   }

   //
   // store walltime in time:
   //
   *time=dt;
   //
   // free the memory on the gpu:
   //
   clReleaseMemObject(devE);
   clReleaseMemObject(devh);
   clReleaseMemObject(devv);
   clReleaseMemObject(deva);
}
