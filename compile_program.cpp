// compiles the OpenCL program
// Return value: 0 ok
//               1 not ok
//
#include <iostream>
#include "compile_program.h"
#include "initialize_gpu.h"

using namespace std;
int compile_program( cl_context &context, cl_program &program)
{
  const char* source = (char*) 
#include "structfacsource.h"
    ;
  //
  // get the list of devices. First we find out how many
  // there are, after that we are able to allocate 
  // enough space for the dvices array:
  //

  cl_int createrc;
  //
  // create a program object
  //
  program = clCreateProgramWithSource(context, // OpenCL context
                                      1,       // number of pointers to strings
                                      &source, // source 
                                      0,       // length of source (0 terminated)
                                      &createrc// errorcode
                                     );
  if (program == NULL || createrc != CL_SUCCESS)
  {
    cout << "Invalid program (NULL)" << endl;
    return 1;
  }
  //
  // compile options:
  //
  const char * options = (const char*) "-cl-fast-relaxed-math";
  //
  // compile the program:
  //
  cl_int buildrc = clBuildProgram (program, // the program
                                   0,       // number of devices, 0 means all
                                   0,       // device list, not needed here 
                                   options, // compile options
                                   0,       // call back function, not used here
                                   0        // user data, not used here
                                  );
  if (buildrc != CL_SUCCESS)
  {
    cout << "Compilation not successful" << endl;
    // 
    // compilation not successful
    // get error log and print it
    //

    //
    // two steps: get first the size of the log, then get the log
    //
    size_t actlogsize;
    clGetProgramBuildInfo(program  // program
        ,devices[0]                // device
        ,CL_PROGRAM_BUILD_LOG      // param name
        ,0                         // param size
        ,0                         // param value (ignored of zero)
        ,&actlogsize               // param_value_size_ret
        );

    size_t logsize = actlogsize;   // size of error log
    char *log      = new char[logsize];

    clGetProgramBuildInfo(program  // program
        ,devices[0]                // device
        ,CL_PROGRAM_BUILD_LOG      // param name see pg 98
        ,logsize                   // param size
        ,log                       // param value
        ,&actlogsize               // param_value_size_ret
        );
    //
    // print the error log:
    //
    cout << log;
    delete [] log;
    return 1;
  }
  return 0;
}
