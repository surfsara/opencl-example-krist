#include <iostream>
#include "initialize_gpu.h"
#include "compile_program.h"
#include "rc2str.h"

//
// Initializes the gpu device, define some important gpu-related
// global variables
// return 0 if ok
//
cl_context       context;             // context
cl_command_queue cmdqueue;            // command queue
cl_kernel        kernel;              // kernel
size_t           max_work_group_size; // max work group size
cl_uint          num_platforms;       // number of OpenCL platforms
cl_platform_id   *platforms;          // the available OpenCL platforms
cl_uint          num_devices;         // the number of GPU devices
cl_device_id     *devices;            // the available gpu devices


using namespace std;

// 
// we are going to call this function from a C program, hence 
// extern "C"
//
extern "C" int initialize_gpu(void)
{

  bool quiet=false;
  cl_int rc;
  //
  // Look around to get the necessary info about the available devices
  //
  // get the available OpenCL platforms. First we obtain the
  // number of platforms, then the platforms itself
  //
  rc = clGetPlatformIDs (0,             // number of platforms asked for
                         0,             // the platforms
                         &num_platforms // how many paltforms are found
                        );
  platforms = new cl_platform_id[num_platforms];
  rc |= clGetPlatformIDs (num_platforms, // number of platforms asked for
                          platforms,     // the platforms
                          0              // how many platforms are found
                         );
  if ( rc != CL_SUCCESS || num_platforms == 0)
  {
    cout << "No platform found" << endl;
    return 1;
  }
  if(!quiet)
  {
    cout << "Number of platforms found: " << num_platforms << endl;
  }

  //
  // get the gpu devices: first the number of devices, then the devices
  //
  rc = clGetDeviceIDs (platforms[0],        // platforms
                       CL_DEVICE_TYPE_GPU,  // requested device type
                       0,                   // max number of devices to return
                       0,                   // devices
                       &num_devices         // number of devices found
                      );
  devices = new cl_device_id[num_devices];
  rc |= clGetDeviceIDs (platforms[0],        // platforms
                        CL_DEVICE_TYPE_GPU,  // requested device type
                        num_devices,         // max number of devices to return
                        devices,             // devices
                        0                    // number of devices found
                       );
  if (num_devices == 0 || rc != CL_SUCCESS)
  {
    cout << "No devices found" << endl;
    return 1;
  }
  if(!quiet)
  {
    cout << "Number of devices found: " << num_devices << endl;
  }

  cl_context_properties *properties = new cl_context_properties[3];
  properties[0] = CL_CONTEXT_PLATFORM;
  properties[1] = (cl_context_properties)platforms[0];
  properties[2] = 0;
  // 
  // create context
  //
  context = clCreateContextFromType (properties,         // properties
                                     CL_DEVICE_TYPE_GPU, // device type
                                     0,                  // call back function
                                     0,                  // user data
                                     &rc                 // error code
                                    );
  if (context == 0 || rc != 0)
  {
    cout << "No gpu detected:" << __FILE__ << ":" << __LINE__ << endl;
    cout << "Context:" << context << " rc:" << rc ;
    string s;
    rcclCreateContextFromType2str(rc, s);
    cout << " " << s << endl;
    return 1;
  }
  delete[] properties;
  //
  // create a commandqueue 'cmdqueue':
  //
  cmdqueue = clCreateCommandQueue (context,    // context
                                   devices[0], // for device 0
                                   0,          // properties (not used) 
                                   &rc         // error code
                                  );
  if (rc != CL_SUCCESS)
  {
    cout << "Cannot create command queue" << endl;
    return rc;
  }
  //
  // compile the program
  //
  cl_program program;
  int compilerc = compile_program(context, program);
  if ( compilerc != 0)
  {
    cout << "Cannot compile program" << endl;
    return compilerc;
  }
  //
  // create kernel
  //
  kernel = clCreateKernel(program,       // program
                          "clstructfac", // name of the program
                          &rc            // error code
                         );
  if (!kernel || rc != CL_SUCCESS)
  {
    cout << "Cannot create kernel" << endl;
    return 1;
  }
  //
  // find out the maximum number of threads in a group (block)
  // max_work_group_size
  //
  rc = clGetKernelWorkGroupInfo(kernel,                      // kernel
                                devices[0],                  // first device
                                CL_KERNEL_WORK_GROUP_SIZE,   // this is what we want to know
                                sizeof(max_work_group_size), // number of bytes  
                                &max_work_group_size,        // put info in here
                                0                            // size of returned item, not used here
                               );
  if (rc != CL_SUCCESS)
  {
    cout << "Problem with calling clGetKernelWorkGroupInfo, "
            "program stops here." << endl;
    return 1;
  }
  cout << "max_work_group_size:" << max_work_group_size << endl;
  free (devices);
  return 0;
}
