#ifndef INITIALIZE_GPU_H
#define INITIALIZE_GPU_H
#ifdef __cplusplus
#include <CL/cl.h>

extern cl_context       context;             // context
extern cl_command_queue cmdqueue;            // cmdqueue
extern cl_kernel        kernel;              // kernel
extern size_t           max_work_group_size; // max work group size
extern cl_uint          num_platforms;       // number of OpenCL platforms
extern cl_platform_id   *platforms;          // the available OpenCL platforms
extern cl_uint          num_devices;         // the number of GPU devices
extern cl_device_id     *devices;            // the available gpu devices
extern "C" int initialize_gpu(void);
#else
int initialize_gpu(void);
#endif
#endif
