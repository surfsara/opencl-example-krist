PROGRAMS = krist
CXX = g++
CC = gcc

SRCS = compiletest.cpp compile_program.cpp initialize_gpu.cpp rc2str.cpp\
       clocks.c krist.c cstructfac.cpp initialize_gpu.cpp structfac.c
OBJS = $(SRCS:.cpp=.o) $(SRCS:.c=.o)

CXXFLAGS = -Wall -g -O3
CFLAGS = -Wall -g -O3

LIBS = -lOpenCL

COMPILETESTOBJS := compiletest.o compile_program.o initialize_gpu.o rc2str.o

KRISTOBJS := krist.o clocks.o cstructfac.o initialize_gpu.o compile_program.o \
             structfac.o rc2str.o

all: 	$(PROGRAMS)

%.o : %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $<

compiletest: $(COMPILETESTOBJS)
	$(CXX) $(CXXFLAGS) -o $@ $(COMPILETESTOBJS) $(LIBS)

krist : $(KRISTOBJS) compiletest
	./compiletest
	$(CXX) -$(CFLAGS) -o $@ $(KRISTOBJS) $(LIBS)

structfacsource.h: structfac.cl
	./cl2h $< > $@

clean:
	rm -f *.o core .depend structfacsource.hh structfacsource.h $(PROGRAMS) compiletest

ifneq ($(MAKECMDGOALS),clean)
include .depend
endif

dep .depend:
	if [ ! -e structfacsource.h ] ; then touch structfacsource.h structfacsource.hh ; fi
	$(CXX) $(CXXFLAGS) -M $(SRCS) > .depend
	if [ -e structfacsource.hh ] ; then rm -f structfacsource.h; fi ; rm -f structfacsource.hh

