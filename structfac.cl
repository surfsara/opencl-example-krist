// this is the gpu version of structfac.c 
// essentially, the outer i-loop is removed, every i value
// is covered by one thread.

kernel void clstructfac(int na,  // number of atoms
    int nr,                      // number of reflections
    global const float4* a,      // input atoms (atomic_number, x, y, z)
    global const float4* h,      // input miller indices (h, k, l)
    global float2*       E,      // output structure factors (Re, Im)
    global float*        v)      // inout for debugging
{
  // get the i-value of this thread:
  int i = get_global_id(0);
  if ( i >= nr)
    return;
  float A,B;
  const float twopi = 6.28318584f;

  float f2 = 0.0f;
  A = 0.0f;
  B = 0.0f;
  float4 hi = h[i];
  for (int j=0; j<na; j++)
  {
     float A1,B1;
     float4 aj = a[j];
     f2 += aj.x*aj.x;
     float arg = twopi*(hi.x*aj.y +
                        hi.y*aj.z +
                        hi.z*aj.w);
     B1 = native_sin(arg);
     A1 = native_cos(arg);
     A += aj.x*A1;
     B += aj.x*B1;
  }
  f2       = native_rsqrt(f2);
  float2 Ei;
  Ei.x = A*f2;
  Ei.y = B*f2;
  E[i] = Ei;
}

// vim: set filetype=c :

