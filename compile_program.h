#ifndef COMPILE_PROGRAM_H
#define COMPILE_PROGRAM_H
#include <CL/cl.h>
int compile_program( cl_context &context, cl_program &program);
#endif
