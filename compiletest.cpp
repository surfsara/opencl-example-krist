// This program compiles the OpenCL program.
// Used to test if the OpenCL program is compilable.
// Return code: 0  no errors
//              1  errors 
#include <iostream>
#include "compile_program.h"
#include "initialize_gpu.h"

using namespace std;
int main()
{
  cout << "Running gpu compile test" << endl;

  int rc = initialize_gpu();
  if (rc != 0)
  {
    cout << "Cannot initialize gpu" << endl;
    exit(1);
  }
  cl_program program;
  //
  // compile the program
  //
  rc = compile_program(context, program);
  if (rc)
  {
    cout << "GPU compile error" << endl;
    exit(1);
  }
  else
  {
    cout << "GPU compile ok" << endl;
    exit(0);
  }
}
