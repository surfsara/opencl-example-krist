#include <math.h>
#include "structfac.h"

/* the reference code for the structure factor computation.
   In the .cl file is the gpu version. */

/* there are some severe problems with the performance of sinf and cosf on
 * amd64 architecture. Therefore we use wrappers
 */

float mysinf(const float x)
{
  double xx = x;
  return (float) sin(xx);
}
float mycosf(const float x)
{
  double xx = x;
  return (float) cos(xx);
}

void structfac(int na, int nr, float*a, float*h, float*E)
{
  int i,j;
  float A,B,twopi;
  twopi = 6.28318584f;

  float f2 = 0.0f;
  for (i=0; i<na; i++)
     f2 += a[4*i]*a[4*i];
  f2 = 1.0f/sqrtf(f2);
    
  for (i=0; i<nr; i++)
  {
    A=0.0f;
    B=0.0f;
    for (j=0; j<na; j++)
    {
      float A1,B1;
      float arg = twopi*(h[3*i+0]*a[4*j+1] +
                         h[3*i+1]*a[4*j+2] +
                         h[3*i+2]*a[4*j+3]);
      //sincosf(arg, &B1, &A1);
      A1 = mycosf(arg);
      B1 = mysinf(arg);
      A += a[4*j]*A1;
      B += a[4*j]*B1;
    }
    E[2*i]   = A*f2;
    E[2*i+1] = B*f2; 
  }
}

