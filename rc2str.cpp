#include "rc2str.h"
#include <string>
void rcclCreateContextFromType2str(const cl_int rc, std::string &s)
{
  switch (rc) 
    {
      case CL_INVALID_PLATFORM: s = "CL_INVALID_PLATFORM";
		     break;
      case CL_INVALID_VALUE: s = "CL_INVALID_VALUE";
		  break;
      case CL_INVALID_DEVICE_TYPE: s = "CL_INVALID_DEVICE_TYPE";
				   break;
      case CL_DEVICE_NOT_AVAILABLE: s = "_DEVICE_NOT_AVAILABLE";
				    break;
      case CL_DEVICE_NOT_FOUND: s = "CL_DEVICE_NOT_FOUND";
				break;
      case CL_OUT_OF_HOST_MEMORY: s = "CL_OUT_OF_HOST_MEMORY";
				  break;
      default: s = "UNKNOWN";
	       break;
    }
  return;
}
