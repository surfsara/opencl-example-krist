#ifndef CLOCKS_H
#define CLOCKS_H

#ifdef __cplusplus
#define CUDAEXTERN extern "C"
#else
#define CUDAEXTERN
#endif
CUDAEXTERN double cputime(void);
CUDAEXTERN double CPUTIME(void);
CUDAEXTERN double cputime_(void);
CUDAEXTERN double wallclock(void);
CUDAEXTERN double WALLCLOCK(void);
CUDAEXTERN double wallclock_(void);
#undef CUDAEXTERN

#endif
