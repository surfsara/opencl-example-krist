This is the OpenCL version of the structure factor compution.

To ensure that a program using OpenCL can run on every graphical
card that supports OpenCL, compiling of the OpenCL code has
to be done during execution. The OpenCL program will be expressed
as a C++ character string which will be compiled in one of the
first phases of the execution of the program.

To make the OpenCL part more readable and editable, this part is
contained in a .cl file, which will be converted to a string
as part of the make process.

Important files:

cl2h:   a script that converts the input to a character string 
        in C++ format.
	This script is used to convert 'structfac'cl' to 
	'structfacsource.h' which should contain a C++
	character string containing the OpenCL program.

clocks.c clocks.h: trivial timing functions

compile_program.cpp: compiles the OpenCL code.

compile_test.cpp: during the making of the program, a test is performed
                  if the OpenCL program compiles without errors. 
		  compile_test.cpp performs this test.

cstructfac.cpp: The C++ code to call the OpenCL code.

initialize_gpu.cpp: code to initialize the gpu.

krist.c: contains the main program, written in C.

structfac.c: the reference implementation of the structure factor computation.

structfac.cl: the OpenCL code

STEPS TO RUN AN OPENCL CODE

It takes several steps to run an OpenCL code:

 - find out which platforms (OpenCL instances) there are: clGetPlatformIDs()
   in initialize_gpu.cpp
 - find out which devices are available: clGetDeviceIDs() in
   initialize_gpu.cpp
 - create a context: clCreateContextFromType() in initialize_gpu.cpp
 - create a command queue: clCreateCommandQueue() in initialize_gpu.cpp
 - create a program object: clCreateProgramWithSource() in compile_program.cpp
 - compile the program object: clBuildProgram() in compile_program.cpp
 - get the error log: clGetProgramBuildInfo() in compile_program.cpp
 - create a kernel: clCreateKernel() in initialize_gpu.cpp
 - find out the max number of threads in a group: clGetKernelWorkGroupInfo() 
   in initialize_gpu.cpp
 - allocate memory on the gpu: clCreateBuffer() in cstructfac.cpp
 - write data to the gpu: clCreateBuffer() and clEnqueueWriteBuffer()
   in cstructfac.cpp
 - start the gpu kernel: clEnqueueNDRangeKernel() in cstructfac.cpp
 - read data from the gpu: clEnqueueReadBuffer() in cstructfac.cpp
 - release memory on the gpu: clReleaseMemObject() in cstructfac.cpp

   
